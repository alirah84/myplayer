package com.ar.player;

import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.ar.player.base.BaseActivity;
import com.ar.player.home.HomeFragment;
import com.ar.player.myplaylist.MyPlayListFragment;
import com.ar.player.search.SearchFragment;
import com.ar.player.search.albumsearch.AlbumSearchFragment;
import com.ar.player.search.tracksearch.TrackSearchFragment;
import com.ar.player.search.videosearch.VideoSearchFragment;
import com.ss.bottomnavigation.BottomNavigation;
import com.ss.bottomnavigation.events.OnSelectedItemChangeListener;

import java.util.Stack;

public class MainActivity extends BaseActivity {
    private View progressBarContainerFrameLayout;
    private FrameLayout noInternetConnection;
    private BottomNavigation bottomNavigation;
    private Stack<Integer> horizontalStack = new Stack<>();
    private Fragment homeFragment;
    private Fragment searchFragment;
    private Fragment trackSearchFragment;
    private Fragment videoSearchFragment;
    private Fragment albumSearchFragment;
    private Fragment myPlayListFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        noInternetConnection = findViewById(R.id.frameLayout_main_noInternetConnection);
        Button refreshBtn = findViewById(R.id.btn_main_refresh);
        if(isInternetAvailable()){
            setupViews();
        }
        else {
            noInternetConnection.setVisibility(View.VISIBLE);
        }
        refreshBtn.setOnClickListener(view -> {
            if (isInternetAvailable()){
                setupViews();
                noInternetConnection.setVisibility(View.GONE);
            }
        });
    }

    public void searchTransaction(Fragment fragment){
        if (fragment instanceof TrackSearchFragment){
            this.trackSearchFragment=fragment;
            horizontalStack.add(R.id.fragment_track_search);
        }
        else if (fragment instanceof VideoSearchFragment){
            this.videoSearchFragment=fragment;
            horizontalStack.add(R.id.fragment_music_video_search);
        }
        else if (fragment instanceof AlbumSearchFragment){
            this.albumSearchFragment=fragment;
            horizontalStack.add(R.id.fragment_album_search);
        }
        replaceTransaction(fragment);
    }

    private void setupViews() {
       ProgressBar MainProgressBar = findViewById(R.id.progressBar_main);
        MainProgressBar.getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN );
        progressBarContainerFrameLayout = findViewById(R.id.frameLayout_progressBarContainer);
      bottomNavigation = findViewById(R.id.bottom_navigation);
      bottomNavigation.setOnSelectedItemChangeListener(new OnSelectedItemChangeListener() {
          @Override
          public void onSelectedItemChanged(int itemId) {
              horizontalStack.add(itemId);
              switch (itemId) {
                  case R.id.tab_home:
                      if (homeFragment == null)
                          homeFragment = new HomeFragment();
                      replaceTransaction(homeFragment);
                      break;
                  case R.id.tab_search:
                      if (searchFragment == null)
                          searchFragment = new SearchFragment();
                      replaceTransaction(searchFragment);
                      break;
                      case R.id.tab_myPlaylist:
                      if (myPlayListFragment == null)
                          myPlayListFragment = new MyPlayListFragment();
                      replaceTransaction(myPlayListFragment);
                      break;
              }

          }
      });
    }

    public void replaceTransaction(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout_fragmentContainer, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void setProgressIndicator(boolean shouldShow) {
        progressBarContainerFrameLayout.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (horizontalStack.size() > 1) {
            horizontalStack.pop();
            switch (horizontalStack.peek()) {
                case R.id.tab_home:
                    replaceTransaction(homeFragment);
                    bottomNavigation.setSelectedItemWithoutNotifyListener(0);
                    break;
                case R.id.tab_search:
                    replaceTransaction(searchFragment);
                    bottomNavigation.setSelectedItemWithoutNotifyListener(1);
                    break;
                case R.id.fragment_track_search:
                    replaceTransaction(trackSearchFragment);
                    bottomNavigation.setSelectedItemWithoutNotifyListener(1);
                    break;
                case R.id.fragment_music_video_search:
                    replaceTransaction(videoSearchFragment);
                    bottomNavigation.setSelectedItemWithoutNotifyListener(1);
                    break;
                    case R.id.fragment_album_search:
                    replaceTransaction(albumSearchFragment);
                    bottomNavigation.setSelectedItemWithoutNotifyListener(1);
                    break;
                    case R.id.tab_myPlaylist:
                    replaceTransaction(myPlayListFragment);
                    bottomNavigation.setSelectedItemWithoutNotifyListener(2);
                    break;
            }
        } else {
            finish();
        }
    }

    private boolean isInternetAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = networkInfo!=null && networkInfo.isConnected();
        return isConnected;
    }

//    public class ConnectivityListener extends BroadcastReceiver {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//          ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
//         NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
//         boolean isConnected = networkInfo!=null && networkInfo.isConnected();
//         if(isConnected){
//             setupViews();
//         }
//         else{
//             noInternetConnection.setVisibility(View.VISIBLE);
//         }
//
//        Button refreshBtn = findViewById(R.id.btn_main_refresh);
//         refreshBtn.setOnClickListener(new View.OnClickListener() {
//             @Override
//             public void onClick(View view) {
//                 if (isConnected)
//             }
//         });
//
//        }
//    }
}
