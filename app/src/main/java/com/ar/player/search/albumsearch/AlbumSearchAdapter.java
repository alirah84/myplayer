package com.ar.player.search.albumsearch;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.album.AlbumPlayerActivity;
import com.ar.player.data.Album;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AlbumSearchAdapter extends RecyclerView.Adapter<AlbumSearchAdapter.AlbumSearchViewHolder> {
    private List<Album> albums;

    public AlbumSearchAdapter(List<Album> albums) {
        this.albums = albums;
    }

    @NonNull
    @Override
    public AlbumSearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album_search,parent,false);
        return new AlbumSearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumSearchViewHolder albumSearchViewHolder, int position) {
        albumSearchViewHolder.bindAlbum(albums.get(position));
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public class AlbumSearchViewHolder extends RecyclerView.ViewHolder {
        private ImageView coverIv;
        private TextView albumNameTv;
        private TextView artistNameTv;
        public AlbumSearchViewHolder(@NonNull View itemView) {
            super(itemView);
            albumNameTv = itemView.findViewById(R.id.tv_albumSearch_titleAlbum);
            artistNameTv = itemView.findViewById(R.id.tv_albumSearch_artistName);
            coverIv = itemView.findViewById(R.id.iv_albumSearch_coverAlbum);
        }

        public void bindAlbum(Album album) {
            albumNameTv.setText(album.getTitle());
            artistNameTv.setText(album.getArtist());
            Picasso.get().load(album.getCover()).into(coverIv);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), AlbumPlayerActivity.class);
                    intent.putExtra(AlbumPlayerActivity.EXTRA_KEY_ALBUM,album);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
