package com.ar.player.search.tracksearch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ar.player.R;
import com.ar.player.base.BaseFragment;
import com.ar.player.data.Track;
import com.ar.player.data.TrackRepository;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TrackSearchFragment extends BaseFragment implements TextWatcher {
    public TrackSearchViewModel viewModel;
    private ImageView clearImageView;
    private View noResult;
    private RecyclerView resultRv;
    public CompositeDisposable disposable = new CompositeDisposable();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new TrackSearchViewModel(new TrackRepository(getContext()));
    }

    public void observe(String keyword){
        viewModel.searchInTracks(keyword).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Track>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Track> tracks) {
                        resultRv.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        resultRv.setAdapter(new TrackSearchAdapter(tracks));

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

    }
    @Override
    public void setupViews() {
        resultRv = view.findViewById(R.id.rv_trackSearch_result);
        EditText trackSearchEt = view.findViewById(R.id.et_trackSearch_search);
        clearImageView = view.findViewById(R.id.iv_trackSearch_clear);
        noResult = view.findViewById(R.id.ll_trackSearch_noResult);
        trackSearchEt.addTextChangedListener(this);
        clearImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackSearchEt.setText("");
            }
        });

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_tracks_search;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        if (s.length()>0) {
            observe(s.toString());
            clearImageView.setVisibility(View.VISIBLE);
            resultRv.setVisibility(View.VISIBLE);
        }
        else {
            clearImageView.setVisibility(View.GONE);
            noResult.setVisibility(View.INVISIBLE);
            resultRv.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onStart() {
        super.onStart();
      Disposable d =  viewModel.getNoResultVisibilitySubject().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean ->{
                   noResult.setVisibility( aBoolean ? View.VISIBLE : View.GONE);
                });
      disposable.add(d);
    }

    @Override
    public void onStop() {
        super.onStop();
        disposable.clear();
    }
}
