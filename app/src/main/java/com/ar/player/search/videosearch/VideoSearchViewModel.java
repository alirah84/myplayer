package com.ar.player.search.videosearch;

import com.ar.player.data.MusicVideo;
import com.ar.player.data.TrackDataSource;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

public class VideoSearchViewModel {
    public TrackDataSource trackDataSource;
    public BehaviorSubject<Boolean> noResultVisibilitySubject = BehaviorSubject.create();
    public VideoSearchViewModel(TrackDataSource trackDataSource){
        this.trackDataSource = trackDataSource;
    }
    public Single<List<MusicVideo>> searchInMusicVideos (String keyword){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("keyword",keyword);
        return trackDataSource.searchInMusicVideos(jsonObject).doOnSuccess(musicVideos ->{
            if (musicVideos.isEmpty())
                noResultVisibilitySubject.onNext(true);
            else noResultVisibilitySubject.onNext(false);
        } );
    }

    public BehaviorSubject<Boolean> getNoResultVisibilitySubject(){
        return noResultVisibilitySubject;
    }
}
