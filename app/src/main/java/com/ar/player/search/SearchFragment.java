package com.ar.player.search;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;

import com.ar.player.MainActivity;
import com.ar.player.R;
import com.ar.player.base.BaseFragment;
import com.ar.player.search.albumsearch.AlbumSearchFragment;
import com.ar.player.search.tracksearch.TrackSearchFragment;
import com.ar.player.search.videosearch.VideoSearchFragment;

public class SearchFragment extends BaseFragment implements View.OnClickListener{
    private TrackSearchFragment trackSearchFragment;
    private VideoSearchFragment videoSearchFragment;
    private AlbumSearchFragment albumSearchFragment;
    @Override
    public void setupViews() {
      Button trackSearchBtn = view.findViewById(R.id.btn_search_tracks);
      Button videoSearchBtn = view.findViewById(R.id.btn_search_musicVideos);
      Button albumSearchBtn = view.findViewById(R.id.btn_search_albums);
      trackSearchBtn.setOnClickListener(this);
      videoSearchBtn.setOnClickListener(this);
      albumSearchBtn.setOnClickListener(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_search_tracks:
                if(trackSearchFragment==null){
                    trackSearchFragment = new TrackSearchFragment();
                }
                ((MainActivity) getActivity()).searchTransaction(trackSearchFragment);
                break;
            case R.id.btn_search_musicVideos:
                if(videoSearchFragment==null){
                    videoSearchFragment = new VideoSearchFragment();
                }
                ((MainActivity) getActivity()).searchTransaction(videoSearchFragment);
                break;

            case R.id.btn_search_albums:
                if(albumSearchFragment==null){
                    albumSearchFragment = new AlbumSearchFragment();
                }
                ((MainActivity) getActivity()).searchTransaction(albumSearchFragment);
                break;
        }
    }
}
