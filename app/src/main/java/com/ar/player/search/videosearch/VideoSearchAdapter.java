package com.ar.player.search.videosearch;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.MusicVideo;
import com.ar.player.data.Track;
import com.ar.player.trackplayer.TrackPlayerActivity;
import com.ar.player.videoplayer.VideoPlayerActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class VideoSearchAdapter extends RecyclerView.Adapter<VideoSearchAdapter.VideoSearchViewHolder> {
    public List<MusicVideo> musicVideos;

    public VideoSearchAdapter(List<MusicVideo> musicVideos) {
        this.musicVideos = musicVideos;
    }

    @NonNull
    @Override
    public VideoSearchViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_music_video_search,viewGroup,false);
        return new VideoSearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoSearchViewHolder videoSearchViewHolder, int position) {
        videoSearchViewHolder.bindMusicVideo(musicVideos.get(position));
    }

    @Override
    public int getItemCount() {
        return musicVideos.size();
    }

    public class VideoSearchViewHolder extends RecyclerView.ViewHolder {
        private TextView titleSong;
        private TextView artistName;
        private ImageView cover;
        public VideoSearchViewHolder(@NonNull View itemView) {
            super(itemView);
            titleSong = itemView.findViewById(R.id.tv_musicVideo_titleSong);
            artistName = itemView.findViewById(R.id.tv_musicVideo_artistName);
            cover = itemView.findViewById(R.id.iv_musicVideo_coverSong);

        }

        public void bindMusicVideo(MusicVideo musicVideo) {
            titleSong.setText(musicVideo.getTitle());
            artistName.setText(musicVideo.getArtist());
            Picasso.get().load(musicVideo.getImageLink()).into(cover);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), VideoPlayerActivity.class);
                    intent.putExtra(VideoPlayerActivity.EXTRA_KEY_MUSIC_VIDEO,musicVideo);
                    itemView.getContext().startActivity(intent);
                }
            });

        }
    }
}
