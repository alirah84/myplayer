package com.ar.player.search.tracksearch;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.Track;
import com.ar.player.trackplayer.TrackPlayerActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TrackSearchAdapter extends RecyclerView.Adapter<TrackSearchAdapter.TrackSearchViewHolder> {
    public List<Track> tracks;

    public TrackSearchAdapter(List<Track> tracks) {
        this.tracks = tracks;
    }

    @NonNull
    @Override
    public TrackSearchViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_track_search,viewGroup,false);
        return new TrackSearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrackSearchViewHolder trackSearchViewHolder, int position) {
        trackSearchViewHolder.bindTrack(tracks.get(position));
    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }

    public class TrackSearchViewHolder extends RecyclerView.ViewHolder {
        private TextView titleSong;
        private TextView artistName;
        private ImageView cover;
        public TrackSearchViewHolder(@NonNull View itemView) {
            super(itemView);
            titleSong = itemView.findViewById(R.id.tv_trackSearch_titleSong);
            artistName = itemView.findViewById(R.id.tv_trackSearch_artistName);
            cover = itemView.findViewById(R.id.iv_trackSearch_coverSong);

        }

        public void bindTrack(Track track) {
            titleSong.setText(track.getTitle());
            artistName.setText(track.getArtist());
            Picasso.get().load(track.getImageLink()).into(cover);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), TrackPlayerActivity.class);
                    intent.putExtra(TrackPlayerActivity.EXTRA_KEY_TRACK,track);
                    itemView.getContext().startActivity(intent);
                }
            });

        }
    }
}
