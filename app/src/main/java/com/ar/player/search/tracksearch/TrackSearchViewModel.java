package com.ar.player.search.tracksearch;

import com.ar.player.data.Track;
import com.ar.player.data.TrackDataSource;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

public class TrackSearchViewModel {
    public TrackDataSource trackDataSource;
    public BehaviorSubject<Boolean> noResultVisibilitySubject = BehaviorSubject.create();
    public TrackSearchViewModel(TrackDataSource trackDataSource){
        this.trackDataSource = trackDataSource;
    }
    public Single<List<Track>> searchInTracks (String keyword){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("keyword",keyword);
        return trackDataSource.searchInTracks(jsonObject).doOnSuccess(tracks ->{
            if (tracks.isEmpty())
                noResultVisibilitySubject.onNext(true);
            else noResultVisibilitySubject.onNext(false);
        } );
    }

    public BehaviorSubject<Boolean> getNoResultVisibilitySubject(){
        return noResultVisibilitySubject;
    }
}
