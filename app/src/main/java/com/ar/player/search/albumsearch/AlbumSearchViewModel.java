package com.ar.player.search.albumsearch;

import com.ar.player.data.Album;
import com.ar.player.data.Track;
import com.ar.player.data.TrackDataSource;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

public class AlbumSearchViewModel {
    public TrackDataSource trackDataSource;
    public BehaviorSubject<Boolean> noResultVisibilitySubject = BehaviorSubject.create();
    public AlbumSearchViewModel(TrackDataSource trackDataSource){
        this.trackDataSource = trackDataSource;
    }
    public Single<List<Album>> searchInAlbums (String keyword){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("keyword",keyword);
        return trackDataSource.searchInAlbums(jsonObject).doOnSuccess(albums ->{
            if (albums.isEmpty())
                noResultVisibilitySubject.onNext(true);
            else noResultVisibilitySubject.onNext(false);
        } );
    }

    public BehaviorSubject<Boolean> getNoResultVisibilitySubject(){
        return noResultVisibilitySubject;
    }
}
