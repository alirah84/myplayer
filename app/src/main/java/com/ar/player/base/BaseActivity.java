package com.ar.player.base;

import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {
    public abstract void setProgressIndicator(boolean shouldShow);
}
