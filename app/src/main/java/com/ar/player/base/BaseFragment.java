package com.ar.player.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ar.player.MainActivity;

public abstract class BaseFragment extends Fragment {
    protected View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view==null){
            view = inflater.inflate(getLayoutRes(),container,false);
        }
        setupViews();
        return view;
    }

    public abstract void setupViews();

    public abstract int getLayoutRes();

    public BaseActivity getBaseActivity(){
        return (MainActivity) getActivity();
    }
}
