package com.ar.player.album;

import com.ar.player.data.Album;
import com.ar.player.data.Track;
import com.ar.player.data.TrackDataSource;

import java.util.List;

import io.reactivex.Single;

public class AlbumPlayerViewModel {
    private TrackDataSource trackDataSource;

    public AlbumPlayerViewModel(TrackDataSource trackDataSource) {

        this.trackDataSource = trackDataSource;
    }

    public Single<Album> getAlbum(Long id) {
        return trackDataSource.getAlbumById(id);
    }

    public Single<List<Track>> getFavoriteTrack() {
        return trackDataSource.getFavoriteTrack();
    }

    public void addFavorite(Track track) {
        trackDataSource.addFavorite(track);
    }

    public void deleteFavorite(Track track) {
        trackDataSource.deleteFavorite(track);
    }
}
