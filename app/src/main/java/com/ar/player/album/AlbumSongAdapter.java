package com.ar.player.album;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.Album;
import com.ar.player.data.Track;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AlbumSongAdapter extends RecyclerView.Adapter<AlbumSongAdapter.AlbumSongViewHolder> {
    private Track track;
    private Album album;
    private int positionOfTrack ;
    private onTrackClickListener onTrackClickListener;
    public AlbumSongAdapter(Album album,Track track,onTrackClickListener onTrackClickListener){
        this.album =album;
        this.onTrackClickListener = onTrackClickListener;
        this.track = track;
        for (int i = 0; i < album.getTracks().size(); i++) {
            if(album.getTracks().get(i).getId().longValue()==track.getId().longValue()){
                positionOfTrack = i;
                break;
            }
        }
    }
    @NonNull
    @Override
    public AlbumSongViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_album_tracks,viewGroup,false);
        return new AlbumSongViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumSongViewHolder albumSongViewHolder, int position) {
        albumSongViewHolder.bindTracks(album.getTracks().get(position));
    }

    @Override
    public int getItemCount() {
        return album.getTracks().size();
    }

    public class AlbumSongViewHolder extends RecyclerView.ViewHolder {
        private TextView titleSongTv;
        private TextView albumNameTv;
        private ImageView coverIv;
        private TextView numberOfTrackTv;
        private CardView albumTracksCv;
        public AlbumSongViewHolder(@NonNull View itemView) {
            super(itemView);
          titleSongTv = itemView.findViewById(R.id.tv_albumTracks_titleSong);
          albumNameTv = itemView.findViewById(R.id.tv_albumTracks_albumName);
          numberOfTrackTv = itemView.findViewById(R.id.tv_albumTracks_count);
        coverIv = itemView.findViewById(R.id.iv_albumTracks_coverAlbum);
      albumTracksCv = itemView.findViewById(R.id.cardView_albumTracks_tracks);
        }

        public void bindTracks(Track track) {
            titleSongTv.setText(track.getTitle());
            albumNameTv.setText(album.getTitle());
            Picasso.get().load(track.getImageLink()).into(coverIv);
            numberOfTrackTv.setText(String.valueOf(getAdapterPosition()+1));
            if(getAdapterPosition()== positionOfTrack){
                albumTracksCv.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(),R.color.darkGray));
            }
            else{
                albumTracksCv.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(),R.color.defaultCardViewColor));
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() != positionOfTrack) {
                        onTrackClickListener.onClick(track);
                        notifyItemChanged(positionOfTrack);
                        positionOfTrack = getAdapterPosition();
                        notifyItemChanged(positionOfTrack);
                    }
                }
            });

        }
    }

   public interface onTrackClickListener{
        void onClick(Track track);
   }
}
