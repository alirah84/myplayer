package com.ar.player.album;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.player.R;
import com.ar.player.base.BaseActivity;
import com.ar.player.data.Album;
import com.ar.player.data.Track;
import com.ar.player.data.TrackRepository;
import com.ar.player.trackplayer.MusicPlayerService;
import com.ar.player.trackplayer.TrackPlayerActivity;
import com.inthecheesefactory.thecheeselibrary.widget.AdjustableImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AlbumPlayerActivity extends BaseActivity implements ServiceConnection,AlbumSongAdapter.onTrackClickListener {
    private MediaPlayer mediaPlayer;
    private ImageView playButton;
    private ImageView favoriteIv;
    private SeekBar seekBar;
    private Timer timer;
    private TextView currentDuration;
    private TextView durationTv;
    private ProgressBar durationProgressBar;
    public Track track;
    public Album album;
    public static final String EXTRA_KEY_ALBUM = "album";
    public static final String EXTRA_KEY_TO_SERVICE_ALBUM = "album_to_service";
//    public static final String EXTRA_KEY_TO_SERVICE = "track_to_service";
    public static final String EXTRA_KEY_TRACK_HAVE_ALBUM_ID = "album_id";
    public static final String EXTRA_KEY_FROM_SERVICE_TO_ACTIVITY ="showActivity";
    public static final String EXTRA_KEY_FROM_SERVICE_TO_ACTIVITY_ALBUM ="showAlbumActivity";
    private AdjustableImageView coverIv;
    private TextView titleTv;
    private TextView artistNameTv;
    private TextView dateTv;
    private Handler handler = new Handler();
    private RecyclerView relatedSongsRv;
    private AlbumPlayerViewModel viewModel;
    private List<Track> favoriteTracks = new ArrayList<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private static final String ACTION_PREPARE_MEDIAPLAYER="com.ar.player.ACTION_PREPARE_MEDIAPLAYER";
    private MediaPlayerPrepareBroadCastReceiver receiver = new MediaPlayerPrepareBroadCastReceiver();
    public boolean isFavorite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_player);
        viewModel = new AlbumPlayerViewModel(new TrackRepository(this));
        track = getIntent().getParcelableExtra(AlbumPlayerActivity.EXTRA_KEY_FROM_SERVICE_TO_ACTIVITY);
        album = getIntent().getParcelableExtra(AlbumPlayerActivity.EXTRA_KEY_FROM_SERVICE_TO_ACTIVITY_ALBUM);
        durationProgressBar = findViewById(R.id.progressBar_albumPlayer_totalDuration);
        durationProgressBar.getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN );

        if (!(track instanceof Track)) {
            durationProgressBar.setVisibility(View.VISIBLE);
            album = getIntent().getParcelableExtra(EXTRA_KEY_ALBUM);
            if (album==null){
                track = getIntent().getParcelableExtra(EXTRA_KEY_TRACK_HAVE_ALBUM_ID);
                setupViews2();
                getAlbumById();
                getFavoriteTrack();
            }
            else{
                track = album.getTracks().get(0);
                getFavoriteTrack();
                setupViews2();
                setupRecyclerView();
                Intent intent = new Intent(this, MusicPlayerService.class);
                intent.putExtra(TrackPlayerActivity.EXTRA_KEY_TO_SERVICE, track);
                intent.putExtra(AlbumPlayerActivity.EXTRA_KEY_TO_SERVICE_ALBUM,album);
                startService(intent);
                bindService(new Intent(AlbumPlayerActivity.this, MusicPlayerService.class), this, BIND_AUTO_CREATE);
                registerReceiver(receiver, new IntentFilter(ACTION_PREPARE_MEDIAPLAYER));
            }
        } else {
            bindService(new Intent(AlbumPlayerActivity.this, MusicPlayerService.class), this, BIND_AUTO_CREATE);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setupViews();
                }
            },500);

            setupViews2();
            setupRecyclerView();
            getFavoriteTrack();
            registerReceiver(receiver, new IntentFilter(ACTION_PREPARE_MEDIAPLAYER));
        }
    }

    private void getFavoriteTrack() {
        viewModel.getFavoriteTrack().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Track>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Track> tracks) {
                        favoriteTracks=tracks;
                        recognizeFavoriteTrack();
                        favoriteIv.setVisibility(View.VISIBLE);
                        favoriteIv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if(isFavorite){
                                    favoriteIv.setImageResource(R.drawable.ic_star_border_unselected_24dp);
                                    viewModel.deleteFavorite(track);
                                    isFavorite=false;
                                }
                                else {
                                    favoriteIv.setImageResource(R.drawable.ic_star_gray_24dp);
                                    viewModel.addFavorite(track);
                                    isFavorite=true;
                                    Toast.makeText(AlbumPlayerActivity.this,getString(R.string.player_addedInPlaylistMessage),Toast.LENGTH_SHORT).show();

                                }
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    public void recognizeFavoriteTrack(){
        for (int i = 0; i <favoriteTracks.size() ; i++) {
            if (favoriteTracks.get(i).getId().longValue()==track.getId().longValue()){
                isFavorite=true;
                break;
            }
            else{
                isFavorite=false;
            }
        }

        if (isFavorite)
        {
            favoriteIv.setImageResource(R.drawable.ic_star_gray_24dp);
        }
        else
        {
            favoriteIv.setImageResource(R.drawable.ic_star_border_unselected_24dp);
        }
    }

    private void getAlbumById() {
        viewModel.getAlbum(track.getAlbumId()).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Album>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Album album) {
                        AlbumPlayerActivity.this.album = album;
                        setupRecyclerView();
                        Intent intent = new Intent(AlbumPlayerActivity.this, MusicPlayerService.class);
                        intent.putExtra(TrackPlayerActivity.EXTRA_KEY_TO_SERVICE, track);
                        intent.putExtra(AlbumPlayerActivity.EXTRA_KEY_TO_SERVICE_ALBUM,album);
                        startService(intent);
                        bindService(new Intent(AlbumPlayerActivity.this, MusicPlayerService.class), AlbumPlayerActivity.this, BIND_AUTO_CREATE);
                        registerReceiver(receiver, new IntentFilter(ACTION_PREPARE_MEDIAPLAYER));

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    public void setupRecyclerView(){
        relatedSongsRv = findViewById(R.id.rv_albumPlayer_relatedSongs);
        relatedSongsRv.setLayoutManager(new LinearLayoutManager(AlbumPlayerActivity.this,LinearLayoutManager.VERTICAL,false));
        relatedSongsRv.setAdapter(new AlbumSongAdapter(album,track,AlbumPlayerActivity.this));
    }

    public void setupViews2(){
        seekBar = findViewById(R.id.sb_albumPlayer_seek);
        seekBar.getThumb().setColorFilter(ContextCompat.getColor(this,R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        coverIv = findViewById(R.id.iv_albumPlayer_cover);
        titleTv = findViewById(R.id.tv_albumPlayer_title);
        favoriteIv = findViewById(R.id.iv_albumPlayer_favorite);
        titleTv.setSelected(true);
        artistNameTv = findViewById(R.id.tv_albumPlayer_artistName);
        artistNameTv.setSelected(true);
        dateTv = findViewById(R.id.tv_albumPlayer_date);
        titleTv.setText(track.getTitle());
        artistNameTv.setText(track.getArtist());
        dateTv.setText(track.getCreatedAt());
        Picasso.get().load(track.getImageLink()).into(coverIv);
      ImageView backIv = findViewById(R.id.iv_albumPlayer_back);
      backIv.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              finish();
          }
      });
    }
    public void setupViews() {
        playButton = findViewById(R.id.iv_albumPlayer_play);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.pause();
                    playButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_play,null));
                }
                else {
                    mediaPlayer.start();

                    playButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_pause,null));

                }
            }
        });
        durationTv = findViewById(R.id.tv_albumPlayer_duration);
        if(durationTv.getVisibility()==View.GONE){
            durationTv.setVisibility(View.VISIBLE);
        }
        try{
            durationTv.setText(formatDuration(mediaPlayer.getDuration()));
        }
        catch (IllegalStateException e){
            e.printStackTrace();
        }

        currentDuration = findViewById(R.id.tv_albumPlayer_currentDuration);
        currentDuration.setText(formatDuration(0));

        ImageView forwardButton = findViewById(R.id.iv_albumPlayer_forward);
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 5000);
            }
        });

        ImageView rewindButton = findViewById(R.id.iv_albumPlayer_rewind);
        rewindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 5000);
            }
        });
        seekBar = findViewById(R.id.sb_albumPlayer_seek);
        try {
            seekBar.setMax(mediaPlayer.getDuration());
        }
        catch (IllegalStateException e){
            e.printStackTrace();
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        timer = new Timer();
        timer.schedule(new AlbumPlayerActivity.MainTimer(),0,1000);
    }

    private String formatDuration(long duration) {
        int seconds = (int) (duration/1000);
        int minutes = seconds/60;
        seconds%=60;
        return String.format(Locale.ENGLISH, "%02d", minutes) + ":" + String.format(Locale.ENGLISH, "%02d", seconds);
    }

    @Override
    public void setProgressIndicator(boolean shouldShow) {

    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        MusicPlayerService.MusicPlayerBinder musicPlayerBinder = (MusicPlayerService.MusicPlayerBinder) iBinder;
        MusicPlayerService musicPlayerService = musicPlayerBinder.getService();
        mediaPlayer = musicPlayerService.getMediaPlayer();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }

    @Override
    public void onClick(Track track) {
        this.track=track;
        recognizeFavoriteTrack();

        durationProgressBar.setVisibility(View.VISIBLE);
        durationTv = findViewById(R.id.tv_albumPlayer_duration);
        durationTv.setVisibility(View.GONE);
        unbindService(this);
        if(timer!=null){
            timer.cancel();
            timer.purge();
        }
        titleTv.setText(track.getTitle());
        artistNameTv.setText(track.getArtist());
        dateTv.setText(track.getCreatedAt());
        Intent intent = new Intent(this,MusicPlayerService.class);
        intent.putExtra(TrackPlayerActivity.EXTRA_KEY_TO_SERVICE,track);
        intent.putExtra(AlbumPlayerActivity.EXTRA_KEY_TO_SERVICE_ALBUM,album);
        startService(intent);
        bindService(new Intent(AlbumPlayerActivity.this,MusicPlayerService.class),AlbumPlayerActivity.this,BIND_AUTO_CREATE);
        Picasso.get().load(track.getImageLink()).into(coverIv);
    }

    public class MainTimer extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(mediaPlayer.getCurrentPosition()==0){
                            playButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_play,null));
                        }
                        seekBar.setProgress(mediaPlayer.getCurrentPosition());
                        currentDuration.setText(formatDuration(mediaPlayer.getCurrentPosition()));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        if(timer!=null){
            timer.cancel();
            timer.purge();
        }
        unbindService(this);
        compositeDisposable.clear();
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    public class MediaPlayerPrepareBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            durationProgressBar.setVisibility(View.GONE);
            setupViews();
        }
    }
}
