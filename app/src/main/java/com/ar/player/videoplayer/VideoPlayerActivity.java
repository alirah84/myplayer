package com.ar.player.videoplayer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.MusicVideo;
import com.ar.player.data.TrackRepository;
import com.ar.player.trackplayer.RelatedSongAdapter;
import com.inthecheesefactory.thecheeselibrary.widget.AdjustableImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class VideoPlayerActivity extends AppCompatActivity implements View.OnClickListener,RelatedVideoAdapter.onVideoClickListener{
    public static final String EXTRA_KEY_MUSIC_VIDEO="music_video";
    private MusicVideo musicVideo;
    private CompositeDisposable disposable = new CompositeDisposable();
    private VideoPlayerViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        musicVideo = getIntent().getParcelableExtra(EXTRA_KEY_MUSIC_VIDEO);
        setupViews();
        viewModel = new VideoPlayerViewModel(new TrackRepository(this));
        observe();
    }

    private void setupViews(){
        AdjustableImageView cover = findViewById(R.id.iv_videoPlayer_cover);
        TextView title = findViewById(R.id.tv_videoPlayer_title);
        TextView artistName = findViewById(R.id.tv_videoPlayer_artistName);
        TextView dateAdded = findViewById(R.id.tv_videoPlayer_date);
        ImageView playImage = findViewById(R.id.iv_videoPlayer_playImage);
        ImageView playButton = findViewById(R.id.iv_videoPlayer_playButton);
        playButton.setOnClickListener(this);
        playImage.setOnClickListener(this);
        Picasso.get().load(musicVideo.getImageLink()).into(cover);
        title.setText(musicVideo.getTitle());
        artistName.setText(musicVideo.getArtist());
        dateAdded.setText(musicVideo.getCreatedAt());
        ImageView backIv = findViewById(R.id.iv_videoPlayer_back);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void observe(){
        viewModel.getRelatedMusicVideos(musicVideo.getArtistId()).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<MusicVideo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<MusicVideo> musicVideos) {
                        RecyclerView relatedVideosRv = findViewById(R.id.rv_videoPlayer_relatedSongs);
                        relatedVideosRv.setLayoutManager(new LinearLayoutManager(VideoPlayerActivity.this,LinearLayoutManager.VERTICAL,false));
                        relatedVideosRv.setAdapter(new RelatedVideoAdapter(musicVideos,musicVideo,VideoPlayerActivity.this));

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.iv_videoPlayer_playImage || view.getId()==R.id.iv_videoPlayer_playButton){
            Intent intent = new Intent(this,FullScreenVideoActivity.class);
            intent.putExtra(FullScreenVideoActivity.EXTRA_KEY_PLAY_VIDEO,musicVideo);
            startActivity(intent);
        }

    }

    @Override
    public void onClick(MusicVideo musicVideo) {
        this.musicVideo = musicVideo;
        setupViews();
    }
}
