package com.ar.player.videoplayer;

import com.ar.player.data.MusicVideo;
import com.ar.player.data.TrackDataSource;

import java.util.List;

import io.reactivex.Single;

public class VideoPlayerViewModel {
    private TrackDataSource trackDataSource;

    public VideoPlayerViewModel(TrackDataSource trackDataSource){

        this.trackDataSource = trackDataSource;
    }
    public Single<List<MusicVideo>> getRelatedMusicVideos(Long id){
     return trackDataSource.getRelatedMusicVideos(id);
    }
}
