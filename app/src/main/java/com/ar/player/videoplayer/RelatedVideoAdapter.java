package com.ar.player.videoplayer;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.MusicVideo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RelatedVideoAdapter extends RecyclerView.Adapter<RelatedVideoAdapter.RelatedVideoViewHolder> {
    private List<MusicVideo> musicVideos;
    private MusicVideo musicVideo;
    public int positionOfVideo;
    public onVideoClickListener onVideoClickListener;

    public RelatedVideoAdapter(List<MusicVideo> musicVideos,MusicVideo musicVideo,onVideoClickListener onVideoClickListener){
        this.musicVideos = musicVideos;
        this.musicVideo = musicVideo;
        this.onVideoClickListener=onVideoClickListener;
        for (int i = 0; i < musicVideos.size(); i++) {
            if(musicVideos.get(i).getId().longValue()==musicVideo.getId().longValue() ){
                positionOfVideo = i;
                break;
            }
        }
    }
    @NonNull
    @Override
    public RelatedVideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_related_videos,parent,false);
        return new RelatedVideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RelatedVideoViewHolder relatedVideoViewHolder, int position) {
        relatedVideoViewHolder.bindVideos(musicVideos.get(position));
    }

    @Override
    public int getItemCount() {
        return musicVideos.size();
    }

    public class RelatedVideoViewHolder extends RecyclerView.ViewHolder {
        public ImageView coverIv;
        public TextView artistNameTv;
        public TextView titleTv;
        public TextView addedDateTv;
        public CardView relatedVideoCv;
        public RelatedVideoViewHolder(@NonNull View itemView) {
            super(itemView);
            coverIv = itemView.findViewById(R.id.iv_relatedVideo_videoCover);
            titleTv = itemView.findViewById(R.id.tv_relatedVideo_titleVideo);
            artistNameTv = itemView.findViewById(R.id.tv_relatedVideo_artistName);
            addedDateTv = itemView.findViewById(R.id.tv_relatedVideo_addedDate);
            relatedVideoCv = itemView.findViewById(R.id.cardView_related_relatedVideos);
        }

        public void bindVideos(MusicVideo musicVideo) {
            Picasso.get().load(musicVideo.getImageLink()).into(coverIv);
            artistNameTv.setText(musicVideo.getArtist());
            titleTv.setText(musicVideo.getTitle());
            addedDateTv.setText(musicVideo.getCreatedAt());

            if(getAdapterPosition()== positionOfVideo){
                relatedVideoCv.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(),R.color.darkGray));
            }
            else{
                relatedVideoCv.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(),R.color.defaultCardViewColor));
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() != positionOfVideo) {
                        onVideoClickListener.onClick(musicVideo);
                        notifyItemChanged(positionOfVideo);
                        positionOfVideo = getAdapterPosition();
                        notifyItemChanged(positionOfVideo);
                    }
                }
            });
        }
    }

    public interface onVideoClickListener{
        void onClick(MusicVideo musicVideo);
    }
}
