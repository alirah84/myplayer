package com.ar.player.trackplayer;

import com.ar.player.data.Track;
import com.ar.player.data.TrackDataSource;

import java.util.List;

import io.reactivex.Single;

public class TrackPlayerViewModel {
    TrackDataSource trackDataSource;

    public TrackPlayerViewModel(TrackDataSource trackDataSource) {
        this.trackDataSource = trackDataSource;
    }

    public Single<List<Track>> getRelatedSongs(Long id) {
        return trackDataSource.getRelatedSongs(id);
    }

    public Single<List<Track>> getFavoriteTrack() {
        return trackDataSource.getFavoriteTrack();
    }

    public void addFavorite(Track track) {
        trackDataSource.addFavorite(track);
    }

    public void deleteFavorite(Track track) {
        trackDataSource.deleteFavorite(track);
    }
}