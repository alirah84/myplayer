package com.ar.player.trackplayer;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.Track;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RelatedSongAdapter extends RecyclerView.Adapter<RelatedSongAdapter.RelatedSongViewHolder> {
private List<Track> related = new ArrayList<>();
    public Track track;
    public int positionOfTrack;
    public onTrackClickListener onTrackClickListener;
    public RelatedSongAdapter(List<Track> related,Track track,onTrackClickListener onTrackClickListener){
    this.related = related;
        this.track = track;
        this.onTrackClickListener=onTrackClickListener;
        for (int i = 0; i < related.size(); i++) {
            if(related.get(i).getId().longValue()==track.getId().longValue() ){
                positionOfTrack = i;
                break;
            }
        }
    }

    @NonNull
    @Override
    public RelatedSongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_related,parent,false);
        return new RelatedSongViewHolder(view) ;
    }

    @Override
    public void onBindViewHolder(@NonNull RelatedSongViewHolder relatedSongViewHolder, int position) {
        relatedSongViewHolder.bindTrack(related.get(position));
    }

    @Override
    public int getItemCount() {
        return related.size();
    }

    public class RelatedSongViewHolder extends RecyclerView.ViewHolder{
        private TextView titleSongTv;
        private TextView artistTv;
        private ImageView coverIv;
        private ImageView signIv;
        private CardView relatedCv;
        private TextView addedDate;
        public RelatedSongViewHolder(@NonNull View itemView) {
            super(itemView);
           titleSongTv = itemView.findViewById(R.id.tv_related_titleSong);
            artistTv = itemView.findViewById(R.id.tv_related_artistName);
            coverIv = itemView.findViewById(R.id.iv_related_coverSong);
            signIv = itemView.findViewById(R.id.iv_related_sign);
            relatedCv = itemView.findViewById(R.id.cardView_related_relatedSong);
            addedDate = itemView.findViewById(R.id.tv_related_addedDate);
        }
        public void bindTrack(Track track){
            titleSongTv.setText(track.getTitle());
            artistTv.setText(track.getArtist());
            Picasso.get().load(track.getImageLink()).into(coverIv);
            addedDate.setText(track.getCreatedAt());

            if (track.getAlbumId()==null){
                signIv.setImageResource(R.drawable.ic_music_note_gray_24dp);
            }
            else{
                signIv.setImageResource(R.drawable.ic_album_gray_24dp);
            }

            if(getAdapterPosition()== positionOfTrack){
                relatedCv.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(),R.color.darkGray));
            }
            else{
                relatedCv.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(),R.color.defaultCardViewColor));
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() != positionOfTrack) {
                        onTrackClickListener.onClick(track);
                        notifyItemChanged(positionOfTrack);
                        positionOfTrack = getAdapterPosition();
                        notifyItemChanged(positionOfTrack);
                    }
                }
            });
        }
    }

    public interface onTrackClickListener {
        void onClick(Track track);
    }
}
