package com.ar.player.trackplayer;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.ar.player.R;
import com.ar.player.album.AlbumPlayerActivity;
import com.ar.player.data.Album;
import com.ar.player.data.Track;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;

public class MusicPlayerService extends Service {
    private static final String ACTION_PLAY="com.ar.player.PLAY_MUSIC";
    private static final String ACTION_FORWARD="com.ar.player.FORWARD_MUSIC";
    private static final String ACTION_REWIND="com.ar.player.REWIND_MUSIC";
    private static final String ACTION_CLOSE="com.ar.player.CLOSE_SERVICE";
    private static final String ACTION_PREPARE_MEDIAPLAYER="com.ar.player.ACTION_PREPARE_MEDIAPLAYER";
    private MusicPlayerBinder musicPlayerBinder = new MusicPlayerBinder();
    private MediaPlayer mediaPlayer;
    private Track track;
    private Album album;
    private Bitmap largeIcon;
    private Intent showMusicPlayerActivityIntent;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return musicPlayerBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        track = intent.getParcelableExtra(TrackPlayerActivity.EXTRA_KEY_TO_SERVICE);
        album = intent.getParcelableExtra(AlbumPlayerActivity.EXTRA_KEY_TO_SERVICE_ALBUM);
        if (intent.getAction()==null){
            intent.setAction("Ali");
        }
        switch (intent.getAction()){
            case ACTION_PLAY:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                }else {
                    mediaPlayer.start();
                }
                break;
            case ACTION_FORWARD:
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()+5000);
                break;
            case ACTION_REWIND:
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()-5000);
                break;
            case ACTION_CLOSE:
                stopForeground(true);
                stopSelf();
                mediaPlayer.release();
                break;
            default:
                if(album!= null){
                    showMusicPlayerActivityIntent=new Intent(this, AlbumPlayerActivity.class);
                    showMusicPlayerActivityIntent.putExtra(AlbumPlayerActivity.EXTRA_KEY_FROM_SERVICE_TO_ACTIVITY,track);
                    showMusicPlayerActivityIntent.putExtra(AlbumPlayerActivity.EXTRA_KEY_FROM_SERVICE_TO_ACTIVITY_ALBUM,album);
                    showMusicPlayerActivityIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                }
                else {
                    showMusicPlayerActivityIntent=new Intent(this, TrackPlayerActivity.class);
                    showMusicPlayerActivityIntent.putExtra(TrackPlayerActivity.EXTRA_KEY_FROM_SERVICE_TO_ACTIVITY,track);
                    showMusicPlayerActivityIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                }

                Intent playIntent=new Intent(this,MusicPlayerService.class);
                playIntent.setAction(ACTION_PLAY);
                PendingIntent playPendingIntent= PendingIntent.getService(this,0,playIntent,0);

                Intent forwardIntent=new Intent(this,MusicPlayerService.class);
                forwardIntent.setAction(ACTION_FORWARD);
                PendingIntent forwardPendingIntent=PendingIntent.getService(this,0,forwardIntent,0);

                Intent rewindIntent=new Intent(this,MusicPlayerService.class);
                rewindIntent.setAction(ACTION_REWIND);
                PendingIntent rewindPendingIntent= PendingIntent.getService(this,0,rewindIntent,0);

                Intent closeIntent = new Intent(this,MusicPlayerService.class);
                closeIntent.setAction(ACTION_CLOSE);
                PendingIntent closePendingIntent = PendingIntent.getService(this,0,closeIntent,0);

                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                String channelId="9001" ;
                String channelName="Music";
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(channelId,channelName,NotificationManager.IMPORTANCE_DEFAULT);
                    notificationChannel.setLightColor(Color.RED);
                    notificationChannel.enableLights(true);
                    notificationManager.createNotificationChannel(notificationChannel);
                }

                Target target = new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        largeIcon=bitmap;
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                };

                Picasso.get().load(track.getImageLink()).into(target);

                NotificationCompat.Builder notification=new NotificationCompat.Builder(this,channelId)
                        .setSmallIcon(R.drawable.ic_music_notification)
                        .setContentTitle(track.getArtist())
                        .setContentText(track.getTitle())
                        .setOngoing(true)
                        .setLargeIcon(largeIcon)
                        .setContentIntent(PendingIntent.getActivity(this,0,showMusicPlayerActivityIntent,PendingIntent.FLAG_UPDATE_CURRENT))
                        .addAction(R.drawable.ic_fast_rewind_white_24dp, "Rewind", rewindPendingIntent)
                        .addAction(R.drawable.ic_play_arrow_gray_24dp, "Play", playPendingIntent)
                        .addAction(R.drawable.ic_fast_forward_white_24dp, "Forward", forwardPendingIntent)
                        .addAction(R.drawable.ic_close_white_28dp,"Close",closePendingIntent)
                        .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0,1,2))
                        ;

                startForeground(101,notification.build());
                setMediaPlayer();
                break;
        }
        return START_STICKY;
    }

    private void setMediaPlayer() {
        try {
            if (mediaPlayer!=null)
                mediaPlayer.release();
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(this, Uri.parse(track.getDownloadLink()));
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    sendBroadcast(new Intent(ACTION_PREPARE_MEDIAPLAYER));
                }
            });

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.seekTo(0);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        mediaPlayer.release();
        super.onDestroy();
    }

    public MediaPlayer getMediaPlayer(){
        return this.mediaPlayer;
    }

    public class MusicPlayerBinder extends Binder{
        public MusicPlayerService getService(){
            return MusicPlayerService.this;
        }
    }
}
