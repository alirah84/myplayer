package com.ar.player.list.tracklist;

import com.ar.player.data.Track;
import com.ar.player.data.TrackDataSource;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

public class TrackListViewModel {
    private TrackDataSource trackDataSource;
    private BehaviorSubject<Boolean> progressBarVisibilitySubject = BehaviorSubject.create();
    public TrackListViewModel(TrackDataSource trackDataSource){
        this.trackDataSource = trackDataSource;
    }
    public Single<List<Track>> getLatestTracks(){
        progressBarVisibilitySubject.onNext(true);
        return trackDataSource.getLatestTracks().doOnEvent((tracks, throwable) -> progressBarVisibilitySubject.onNext(false));
    }

    public BehaviorSubject<Boolean> getProgressBarVisibilitySubject() {
        return progressBarVisibilitySubject;
    }
}
