package com.ar.player.list.tracklist;

import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ar.player.R;
import com.ar.player.base.BaseActivity;
import com.ar.player.data.Track;
import com.ar.player.data.TrackRepository;
import com.ar.player.home.TrackAdapter;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TrackListActivity extends BaseActivity {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ProgressBar listProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_list);
        TrackListViewModel viewModel = new TrackListViewModel(new TrackRepository(this));
        listProgressBar = findViewById(R.id.progressBar_list_singleList);
        listProgressBar.getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN );
        ImageView backIv = findViewById(R.id.iv_list_back);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

       Disposable d = viewModel.getProgressBarVisibilitySubject().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> setProgressIndicator(aBoolean));
       compositeDisposable.add(d);
        viewModel.getLatestTracks().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Track>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Track> tracks) {
                        TrackAdapter trackAdapter = new TrackAdapter(tracks);
                        RecyclerView listRv = findViewById(R.id.rv_list_singleList);
                        listRv.setLayoutManager(new GridLayoutManager(TrackListActivity.this, 2));
                        listRv.setAdapter(trackAdapter);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Override
    public void setProgressIndicator(boolean shouldShow) {
        listProgressBar.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }
}
