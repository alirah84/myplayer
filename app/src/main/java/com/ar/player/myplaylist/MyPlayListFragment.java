package com.ar.player.myplaylist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.base.BaseFragment;
import com.ar.player.data.Track;
import com.ar.player.data.TrackDataSource;
import com.ar.player.data.TrackRepository;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyPlayListFragment extends BaseFragment implements MyPlayListAdapter.onPlaylistEventCallBack{
    public TextView emptyStateTv;
    public MyPlayListViewModel viewModel = new MyPlayListViewModel(new TrackRepository(getContext()));
    public CompositeDisposable compositeDisposable = new CompositeDisposable();
    public MyPlayListAdapter myPlayListAdapter;
    public TrackDataSource trackDataSource;
    @Override
    public void setupViews() {
        emptyStateTv = view.findViewById(R.id.tv_myPlayList_emptyState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trackDataSource = new TrackRepository(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
       Disposable d = viewModel.getEmptyState().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    emptyStateTv.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                });
        compositeDisposable.add(d);
        viewModel.getFavoriteTrack().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Track>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Track> tracks) {
                        RecyclerView myPlayListRv = view.findViewById(R.id.rv_myPlayList_myPlayList);
                        myPlayListRv.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        myPlayListAdapter = new MyPlayListAdapter(tracks,MyPlayListFragment.this);
                        myPlayListRv.setAdapter(myPlayListAdapter);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_myplaylist;
    }

    @Override
    public void onDeleteClick(Track track) {
        myPlayListAdapter.delete(track);
        trackDataSource.deleteFavorite(track);

    }

    @Override
    public void onGetItemCountCalled(int count) {
        if(count>0)
            emptyStateTv.setVisibility(View.GONE);
        else
            emptyStateTv.setVisibility(View.VISIBLE);
    }
}
