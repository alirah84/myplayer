package com.ar.player.myplaylist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.album.AlbumPlayerActivity;
import com.ar.player.data.Track;
import com.ar.player.trackplayer.TrackPlayerActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyPlayListAdapter extends RecyclerView.Adapter<MyPlayListAdapter.MyPlayListViewHolder> {
    public List<Track> tracks;
    public onPlaylistEventCallBack eventCallBack;
    public MyPlayListAdapter(List<Track> tracks,onPlaylistEventCallBack eventCallBack){
        this.tracks = tracks;
        this.eventCallBack = eventCallBack;
    }
    @NonNull
    @Override
    public MyPlayListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_myplaylist,viewGroup,false);
        return new MyPlayListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyPlayListViewHolder myPlayListViewHolder, int position) {
        myPlayListViewHolder.bindTrack(tracks.get(position));
    }

    @Override
    public int getItemCount() {
       int count = tracks.size();
       eventCallBack.onGetItemCountCalled(count);
        return count;
    }

    public class MyPlayListViewHolder extends RecyclerView.ViewHolder{
        public ImageView coverIv;
        public TextView artistNameTv;
        public TextView songNameTv;
        public ImageView deleteIcon;
        public MyPlayListViewHolder(@NonNull View itemView) {
            super(itemView);
            songNameTv = itemView.findViewById(R.id.tv_myPlayList_titleSong);
            artistNameTv = itemView.findViewById(R.id.tv_myPlayList_artistName);
            coverIv = itemView.findViewById(R.id.iv_myPlayList_cover);
            deleteIcon = itemView.findViewById(R.id.iv_myPlayList_deleteIcon);
        }

        public void bindTrack(Track track) {
            Picasso.get().load(track.getImageLink()).into(coverIv);
            artistNameTv.setText(track.getArtist());
            songNameTv.setText(track.getTitle());
            deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    eventCallBack.onDeleteClick(track);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (track.getAlbumId() != null) {
                        Intent intent = new Intent(itemView.getContext(), AlbumPlayerActivity.class);
                        intent.putExtra(AlbumPlayerActivity.EXTRA_KEY_TRACK_HAVE_ALBUM_ID, track);
                        itemView.getContext().startActivity(intent);
                    }

                    else{
                        Intent intent = new Intent(itemView.getContext(), TrackPlayerActivity.class);
                        intent.putExtra(TrackPlayerActivity.EXTRA_KEY_TRACK, track);
                        itemView.getContext().startActivity(intent);
                    }
                }
            });
        }
    }

    public void delete(Track track) {
        int index = tracks.indexOf(track);
        tracks.remove(index);
        notifyItemRemoved(index);
    }

    public interface onPlaylistEventCallBack{
        void onDeleteClick(Track track);
        void onGetItemCountCalled(int count);
    }
}
