package com.ar.player.myplaylist;

import com.ar.player.data.Track;
import com.ar.player.data.TrackDataSource;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

public class MyPlayListViewModel {
    public BehaviorSubject<Boolean> emptyState = BehaviorSubject.create();
    public TrackDataSource trackDataSource;
    public MyPlayListViewModel(TrackDataSource trackDataSource){
        this.trackDataSource = trackDataSource;
    }
    public Single<List<Track>> getFavoriteTrack(){
        return trackDataSource.getFavoriteTrack().doOnSuccess(tracks -> {
           if (tracks.isEmpty()){
               emptyState.onNext(true);
           }
           else{
               emptyState.onNext(false);
           }
        });
    }

    public BehaviorSubject<Boolean> getEmptyState(){
        return emptyState;
    }

}
