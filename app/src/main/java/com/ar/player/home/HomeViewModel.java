package com.ar.player.home;

import com.ar.player.data.Album;
import com.ar.player.data.Track;
import com.ar.player.data.TrackDataSource;
import com.ar.player.data.MusicVideo;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

public class HomeViewModel {
    private TrackDataSource trackDataSource;
    private BehaviorSubject<Boolean> progressBarVisibilitySubject = BehaviorSubject.create();
    public HomeViewModel(TrackDataSource trackDataSource) {
        this.trackDataSource = trackDataSource;
    }

    public Single<List<Track>> getTracks(){
        progressBarVisibilitySubject.onNext(true);
        return trackDataSource.getTracks().doOnEvent((tracks, throwable) -> progressBarVisibilitySubject.onNext(false));
    }

    public Single<List<MusicVideo>> getMusicVideos(){
        progressBarVisibilitySubject.onNext(true);
        return trackDataSource.getMusicVideos().doOnEvent((videos, throwable) -> progressBarVisibilitySubject.onNext(false));
    }

    public Single<List<Album>> getAlbums(){
        progressBarVisibilitySubject.onNext(true);
        return trackDataSource.getAlbums().doOnEvent((albums, throwable) -> progressBarVisibilitySubject.onNext(false));
    }

    public Single<List<Track>> getBanners(){
        progressBarVisibilitySubject.onNext(true);
        return trackDataSource.getBanners().doOnEvent((tracks, throwable) -> progressBarVisibilitySubject.onNext(false));
    }

    public BehaviorSubject<Boolean> getProgressBarVisibilitySubject() {
        return progressBarVisibilitySubject;
    }
}
