package com.ar.player.home.slider;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.Track;
import com.ar.player.trackplayer.TrackPlayerActivity;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {
    private Context context;
    private List<Track> banners = new ArrayList<>();
    public SliderAdapter(List<Track> banners,Context context){
        this.banners=banners;
        this.context=context;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        viewHolder.bindBanners(banners.get(position));
    }

    @Override
    public int getCount() {
        return banners.size();
    }

    public class SliderAdapterVH extends SliderViewAdapter.ViewHolder {
        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;
        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }

        public void bindBanners(Track track){
            textViewDescription.setText(track.getArtist() + " - " + track.getTitle());
            Picasso.get().load(track.getBannerLink()).into(imageViewBackground);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), TrackPlayerActivity.class);
                    intent.putExtra(TrackPlayerActivity.EXTRA_KEY_TRACK,track);
                    itemView.getContext().startActivity(intent);
                }
            });
        }

    }
}
