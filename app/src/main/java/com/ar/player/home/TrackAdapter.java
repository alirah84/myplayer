package com.ar.player.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.Track;
import com.ar.player.trackplayer.TrackPlayerActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TrackAdapter extends RecyclerView.Adapter<TrackAdapter.TrackViewHolder>{
private List<Track> tracks = new ArrayList<>();

    public TrackAdapter(List<Track> tracks) {
        this.tracks = tracks;
    }

    @NonNull
    @Override
    public TrackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_track,parent,false);
        return new TrackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrackViewHolder holder, int position) {
        holder.bindTracks(tracks.get(position));
    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }

    public class TrackViewHolder extends RecyclerView.ViewHolder {
        private ImageView coverIv;
        private TextView artistNameTv;
        private TextView songNameTv;
        public TrackViewHolder(@NonNull View itemView) {
            super(itemView);
          coverIv = itemView.findViewById(R.id.iv_track_imageArtist);
          artistNameTv = itemView.findViewById(R.id.tv_track_artistName);
          songNameTv = itemView.findViewById(R.id.tv_track_songName);
        }

        public void bindTracks(Track track) {
            Picasso.get().load(track.getImageLink()).into(coverIv);
            artistNameTv.setText(track.getArtist());
            songNameTv.setText(track.getTitle());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), TrackPlayerActivity.class);
                    intent.putExtra(TrackPlayerActivity.EXTRA_KEY_TRACK,track);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}