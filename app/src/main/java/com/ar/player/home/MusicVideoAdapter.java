package com.ar.player.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.data.MusicVideo;
import com.ar.player.videoplayer.VideoPlayerActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MusicVideoAdapter extends RecyclerView.Adapter<MusicVideoAdapter.MusicVideoViewHolder> {
    private List<MusicVideo> musicVideos;
    public MusicVideoAdapter(List<MusicVideo> musicVideos){
        this.musicVideos=musicVideos;
    }
    @NonNull
    @Override
    public MusicVideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_music_video,parent,false);
        return new MusicVideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicVideoViewHolder videoViewHolder, int position) {
        videoViewHolder.bindMusicVideos(musicVideos.get(position));
    }

    @Override
    public int getItemCount() {
        return musicVideos.size();
    }

    public class MusicVideoViewHolder extends RecyclerView.ViewHolder {
        private ImageView cover;
        private TextView artist;
        private TextView title;
        public MusicVideoViewHolder(@NonNull View itemView) {
            super(itemView);
            cover = itemView.findViewById(R.id.iv_musicVideo_cover);
            artist = itemView.findViewById(R.id.tv_musicVideo_artist);
            title = itemView.findViewById(R.id.tv_musicVideo_title);
        }

        public void bindMusicVideos(MusicVideo video){
            Picasso.get().load(video.getImageLink()).into(cover);
            artist.setText(video.getArtist());
            title.setText(video.getTitle());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), VideoPlayerActivity.class);
                    intent.putExtra(VideoPlayerActivity.EXTRA_KEY_MUSIC_VIDEO,video);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
