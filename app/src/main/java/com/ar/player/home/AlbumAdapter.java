package com.ar.player.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.album.AlbumPlayerActivity;
import com.ar.player.data.Album;
import com.ar.player.trackplayer.TrackPlayerActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {
    private final List<Album> albums;

    public AlbumAdapter(List<Album> albums){
        this.albums = albums;
    }
    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_album,viewGroup,false);
        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder albumViewHolder, int position) {
        albumViewHolder.bindAlbum(albums.get(position));

    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder{
        private ImageView cover;
        private TextView artist;
        private TextView title;

        public AlbumViewHolder(@NonNull View itemView) {
            super(itemView);
            cover = itemView.findViewById(R.id.iv_album_cover);
            artist = itemView.findViewById(R.id.tv_album_artist);
            title = itemView.findViewById(R.id.tv_album_title);
        }

        public void bindAlbum(Album album) {
            Picasso.get().load(album.getCover()).into(cover);
            artist.setText(album.getArtist());
            title.setText(album.getTitle());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), AlbumPlayerActivity.class);
                    intent.putExtra(AlbumPlayerActivity.EXTRA_KEY_ALBUM,album);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
