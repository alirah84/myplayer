package com.ar.player.home;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ar.player.R;
import com.ar.player.base.BaseFragment;
import com.ar.player.data.Album;
import com.ar.player.data.Track;
import com.ar.player.data.TrackRepository;
import com.ar.player.data.MusicVideo;
import com.ar.player.home.slider.SliderAdapter;
import com.ar.player.list.tracklist.TrackListActivity;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeFragment extends BaseFragment {
    private HomeViewModel homeViewModel;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private TrackAdapter trackAdapter;
    private boolean isSinglesRendered;
    private boolean isVideosRendered;
    private boolean isAlbumsRendered;
    private boolean isBannersRendered;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeViewModel = new HomeViewModel(new TrackRepository(getContext()));
    }

    @Override
    public void onStart() {
        super.onStart();
        observe();
    }

    private void observe() {
       Disposable d = homeViewModel.getProgressBarVisibilitySubject().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean ->  getBaseActivity().setProgressIndicator(aBoolean) );
       compositeDisposable.add(d);
       if (!isSinglesRendered) {
           homeViewModel.getTracks().subscribeOn(Schedulers.newThread())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(new SingleObserver<List<Track>>() {
                       @Override
                       public void onSubscribe(Disposable d) {
                           compositeDisposable.add(d);
                       }

                       @Override
                       public void onSuccess(List<Track> tracks) {
                           RecyclerView latestMusicRv = view.findViewById(R.id.rv_home_latestMusic);
                           latestMusicRv.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                           trackAdapter = new TrackAdapter(tracks);
                           latestMusicRv.setAdapter(trackAdapter);
                           isSinglesRendered = true;

                       }

                       @Override
                       public void onError(Throwable throwable) {
                           throwable.getMessage();
                       }
                   });
       }

       if(!isVideosRendered) {
           homeViewModel.getMusicVideos().subscribeOn(Schedulers.newThread())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(new SingleObserver<List<MusicVideo>>() {
                       @Override
                       public void onSubscribe(Disposable d) {
                           compositeDisposable.add(d);
                       }

                       @Override
                       public void onSuccess(List<MusicVideo> videos) {
                           RecyclerView musicVideos = view.findViewById(R.id.rv_home_musicVideos);
                           musicVideos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                           MusicVideoAdapter musicVideoAdapter = new MusicVideoAdapter(videos);
                           musicVideos.setAdapter(musicVideoAdapter);
                           isVideosRendered = true;
                       }

                       @Override
                       public void onError(Throwable throwable) {
                           throwable.getMessage();

                       }
                   });
       }
       if (!isBannersRendered) {
           homeViewModel.getBanners().subscribeOn(Schedulers.newThread())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(new SingleObserver<List<Track>>() {
                       @Override
                       public void onSubscribe(Disposable d) {
                           compositeDisposable.add(d);
                       }

                       @Override
                       public void onSuccess(List<Track> tracks) {
                           SliderView sliderView = view.findViewById(R.id.imageSlider);
                           SliderAdapter adapter = new SliderAdapter(tracks, getContext());
                           sliderView.setSliderAdapter(adapter);

                           sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                           sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                           sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                           sliderView.setIndicatorSelectedColor(Color.WHITE);
                           sliderView.setIndicatorUnselectedColor(Color.GRAY);
                           sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
                           sliderView.startAutoCycle();
                           isBannersRendered=true;
                       }

                       @Override
                       public void onError(Throwable e) {

                       }
                   });
       }
       if(!isAlbumsRendered){
           homeViewModel.getAlbums().subscribeOn(Schedulers.newThread())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(new SingleObserver<List<Album>>() {
                       @Override
                       public void onSubscribe(Disposable d) {
                           compositeDisposable.add(d);
                       }

                       @Override
                       public void onSuccess(List<Album> albums) {
                           RecyclerView albumRv = view.findViewById(R.id.rv_home_albums);
                           albumRv.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
                           albumRv.setAdapter(new AlbumAdapter(albums));
                           isAlbumsRendered=true;
                       }

                       @Override
                       public void onError(Throwable e) {

                       }
                   });

       }
    }

    @Override
    public void onStop() {
        super.onStop();
        getBaseActivity().setProgressIndicator(false);
        compositeDisposable.clear();
    }

    @Override
    public void setupViews() {
      TextView seeAllSinglesTv = view.findViewById(R.id.tv_home_seeAll);
      seeAllSinglesTv.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              startActivity(new Intent(getContext(), TrackListActivity.class));
          }
      });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_home;
    }
}
