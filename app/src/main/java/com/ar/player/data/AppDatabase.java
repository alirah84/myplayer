package com.ar.player.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Track.class},version = 1,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public static AppDatabase instance;
    public abstract TrackLocalDataSource getTrackLocalDataSource();
    public static AppDatabase getInstance(Context context){
        if (instance==null){
            instance = Room.databaseBuilder(context,AppDatabase.class,"my_player").allowMainThreadQueries().build();
            return instance;
        }
        return instance;
    }
}
