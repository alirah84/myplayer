package com.ar.player.data;

public class ApiServiceProvider {
    public static ApiService apiService;
    public static ApiService provideApiService(){
        if (apiService==null){
           apiService = RetrofitSingleton.getInstance().create(ApiService.class);
        }
        return apiService;
    }
}
