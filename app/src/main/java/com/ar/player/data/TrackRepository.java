package com.ar.player.data;

import android.content.Context;

import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class TrackRepository implements TrackDataSource {
    public TrackCloudDataSource trackCloudDataSource;
    public TrackLocalDataSource trackLocalDataSource;
    public TrackRepository(Context context){
        trackCloudDataSource = new TrackCloudDataSource();
        trackLocalDataSource = AppDatabase.getInstance(context).getTrackLocalDataSource();
    }
    @Override
    public Single<List<Track>> getTracks() {
        return trackCloudDataSource.getTracks();
    }

    @Override
    public Single<List<Track>> getLatestTracks() {
        return trackCloudDataSource.getLatestTracks();
    }

    @Override
    public Single<List<Track>> getRelatedSongs(Long id) {
        return trackCloudDataSource.getRelatedSongs(id);
    }

    @Override
    public Single<List<MusicVideo>> getRelatedMusicVideos(Long id) {
        return trackCloudDataSource.getRelatedMusicVideos(id);
    }

    @Override
    public Single<List<MusicVideo>> getMusicVideos() {
        return trackCloudDataSource.getMusicVideos();
    }

    @Override
    public Single<List<Track>> getBanners() {
        return trackCloudDataSource.getBanners();
    }

    @Override
    public Single<List<Track>> searchInTracks(JsonObject jsonObject){
        return trackCloudDataSource.searchInTracks(jsonObject);
    }

    @Override
    public Single<List<MusicVideo>> searchInMusicVideos(JsonObject jsonObject) {
        return trackCloudDataSource.searchInMusicVideos(jsonObject);
    }

    @Override
    public Single<List<Album>> searchInAlbums(JsonObject jsonObject) {
        return trackCloudDataSource.searchInAlbums(jsonObject);
    }

    @Override
    public Single<List<Album>> getAlbums() {
        return trackCloudDataSource.getAlbums();
    }

    @Override
    public Single<Album> getAlbumById(Long id) {
        return trackCloudDataSource.getAlbumById(id);
    }

    @Override
    public Single<List<Track>> getFavoriteTrack() {
        return trackLocalDataSource.getFavoriteTrack();
    }

    @Override
    public void addFavorite(Track track) {
        trackLocalDataSource.addFavorite(track);
    }

    @Override
    public void deleteFavorite(Track track) {
         trackLocalDataSource.deleteFavorite(track);
    }
}
