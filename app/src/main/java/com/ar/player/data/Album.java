
package com.ar.player.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Album implements Parcelable {

    @SerializedName("artist")
    private String mArtist;
    @SerializedName("artist_id")
    private Long mArtistId;
    @SerializedName("cover")
    private String mCover;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("id")
    private Long mId;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("tracks")
    private List<Track> mTracks;

    public String getArtist() {
        return mArtist;
    }

    public void setArtist(String artist) {
        mArtist = artist;
    }

    public Long getArtistId() {
        return mArtistId;
    }

    public void setArtistId(Long artistId) {
        mArtistId = artistId;
    }

    public String getCover() {
        return mCover;
    }

    public void setCover(String cover) {
        mCover = cover;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public List<Track> getTracks() {
        return mTracks;
    }

    public void setTracks(List<Track> tracks) {
        mTracks = tracks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mArtist);
        dest.writeValue(this.mArtistId);
        dest.writeString(this.mCover);
        dest.writeString(this.mCreatedAt);
        dest.writeValue(this.mId);
        dest.writeString(this.mTitle);
        dest.writeList(this.mTracks);
    }

    public Album() {
    }

    protected Album(Parcel in) {
        this.mArtist = in.readString();
        this.mArtistId = (Long) in.readValue(Long.class.getClassLoader());
        this.mCover = in.readString();
        this.mCreatedAt = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mTitle = in.readString();
        this.mTracks = new ArrayList<Track>();
        in.readList(this.mTracks, Track.class.getClassLoader());
    }

    public static final Parcelable.Creator<Album> CREATOR = new Parcelable.Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel source) {
            return new Album(source);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };
}
