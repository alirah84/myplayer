package com.ar.player.data;

import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface TrackDataSource {
    Single<List<Track>> getTracks();
    Single<List<Track>> getLatestTracks();
    Single<List<Track>> getRelatedSongs(Long id);
    Single<List<MusicVideo>> getRelatedMusicVideos(Long id);
    Single<List<MusicVideo>> getMusicVideos();
    Single<List<Track>> getBanners();
    Single<List<Track>> searchInTracks(JsonObject jsonObject);
    Single<List<MusicVideo>> searchInMusicVideos(JsonObject jsonObject);
    Single<List<Album>> searchInAlbums(JsonObject jsonObject);
    Single<List<Album>> getAlbums();
    Single<Album> getAlbumById(Long id);
    Single<List<Track>> getFavoriteTrack();
    void addFavorite(Track track);
    void deleteFavorite(Track track);


}
