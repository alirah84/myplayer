package com.ar.player.data;

import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    @GET("music/singles")
    Single<List<Track>> getTracks();

    @GET("music/singleList")
    Single<List<Track>> getLatestTracks();

    @GET("music/relatedSongs/{id}")
    Single<List<Track>> getRelatedSongs(@Path("id") Long id);

    @GET("music/relatedVideos/{id}")
    Single<List<MusicVideo>> getRelatedMusicVideos(@Path("id") Long id);

    @GET("music/video")
    Single<List<MusicVideo>> getMusicVideos();

    @GET("music/albums")
    Single<List<Album>> getAlbums();

    @GET("music/album/{id}")
    Single<Album> getAlbumById(@Path("id") Long id);

    @GET("banner/slider")
    Single<List<Track>> getBanners();

    @POST("search/tracks")
    Single<List<Track>> searchInTracks(@Body JsonObject jsonObject);

    @POST("search/musicVideos")
    Single<List<MusicVideo>> searchInMusicVideos(@Body JsonObject jsonObject);

    @POST("search/albums")
    Single<List<Album>> searchInAlbums(@Body JsonObject jsonObject);
}