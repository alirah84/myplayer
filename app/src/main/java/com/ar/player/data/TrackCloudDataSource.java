package com.ar.player.data;

import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class TrackCloudDataSource implements TrackDataSource {
public ApiService apiService = ApiServiceProvider.provideApiService();
    @Override
    public Single<List<Track>> getTracks() {
        return apiService.getTracks();
    }

    @Override
    public Single<List<Track>> getLatestTracks() {
        return apiService.getLatestTracks();
    }

    @Override
    public Single<List<Track>> getRelatedSongs(Long id) {
        return apiService.getRelatedSongs(id);
    }

    @Override
    public Single<List<MusicVideo>> getRelatedMusicVideos(Long id) {
        return apiService.getRelatedMusicVideos(id);
    }

    @Override
    public Single<List<MusicVideo>> getMusicVideos() {
        return apiService.getMusicVideos();
    }

    @Override
    public Single<List<Track>> getBanners() {
        return apiService.getBanners();
    }

    @Override
    public Single<List<Track>> searchInTracks(JsonObject jsonObject){
        return apiService.searchInTracks(jsonObject);
    }

    @Override
    public Single<List<MusicVideo>> searchInMusicVideos(JsonObject jsonObject) {
        return apiService.searchInMusicVideos(jsonObject);
    }

    @Override
    public Single<List<Album>> searchInAlbums(JsonObject jsonObject) {
        return apiService.searchInAlbums(jsonObject);
    }

    @Override
    public Single<List<Album>> getAlbums() {
        return apiService.getAlbums();
    }

    @Override
    public Single<Album> getAlbumById(Long id) {
        return apiService.getAlbumById(id);
    }

    @Override
    public Single<List<Track>> getFavoriteTrack() {
        return null;
    }

    @Override
    public void addFavorite(Track track) {

    }

    @Override
    public void deleteFavorite(Track track) {

    }
}
