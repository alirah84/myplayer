package com.ar.player.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
@Dao
public abstract class TrackLocalDataSource implements TrackDataSource {
    @Override
    public Single<List<Track>> getTracks() {
        return null;
    }

    @Override
    public Single<List<Track>> getLatestTracks() {
        return null;
    }

    @Override
    public Single<List<Track>> getRelatedSongs(Long id) {
        return null;
    }

    @Override
    public Single<List<MusicVideo>> getRelatedMusicVideos(Long id) {
        return null;
    }
    @Override
    public Single<Album> getAlbumById(Long id) {
        return null;
    }
    @Override
    public Single<List<MusicVideo>> getMusicVideos() {
        return null;
    }

    @Override
    public Single<List<Track>> getBanners() {
        return null;
    }

    @Override
    public Single<List<Track>> searchInTracks(JsonObject jsonObject) {
        return null;
    }

    @Override
    public Single<List<MusicVideo>> searchInMusicVideos(JsonObject jsonObject) {
        return null;
    }

    @Override
    public Single<List<Album>> searchInAlbums(JsonObject jsonObject) {
        return null;
    }

    @Override
    public Single<List<Album>> getAlbums() {
        return null;
    }
    @Query("SELECT * FROM tbl_track")
    @Override
    public abstract Single<List<Track>> getFavoriteTrack();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    @Override
    public abstract void addFavorite(Track track);

    @Delete
    @Override
    public abstract void deleteFavorite(Track track);
}
