package com.ar.player.data;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitSingleton {
    public static Retrofit retrofit;
    public static Retrofit getInstance(){
        if(retrofit==null){
            OkHttpClient.Builder client = new OkHttpClient.Builder();
            client.connectTimeout(5, TimeUnit.MINUTES);
            client.readTimeout(5, TimeUnit.MINUTES);
            client.writeTimeout(5, TimeUnit.MINUTES);
         retrofit = new Retrofit.Builder()
                    .baseUrl("http://vitamineee.com/api/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                 .client(client.build())
                    .build();
        }
        return retrofit;
    }
    private RetrofitSingleton(){
    }
}
