
package com.ar.player.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
@Entity(tableName = "tbl_track")
public class Track implements Parcelable {

    @ColumnInfo(name = "album_id")
    @SerializedName("album_id")
    private Long mAlbumId;
    @SerializedName("artist")
    private String mArtist;
    @ColumnInfo(name = "artist_id")
    @SerializedName("artist_id")
    private Long mArtistId;
    @ColumnInfo(name = "banner_link")
    @SerializedName("banner_link")
    private String mBannerLink;
    @ColumnInfo(name = "created_at")
    @SerializedName("created_at")
    private String mCreatedAt;
    @ColumnInfo(name = "download_link")
    @SerializedName("download_link")
    private String mDownloadLink;
    @SerializedName("id")
    @PrimaryKey
    private Long mId;
    @ColumnInfo(name = "image_link")
    @SerializedName("image_link")
    private String mImageLink;
    @SerializedName("title")
    private String mTitle;

    public Long getAlbumId() {
        return mAlbumId;
    }

    public void setAlbumId(Long albumId) {
        mAlbumId = albumId;
    }

    public String getArtist() {
        return mArtist;
    }

    public void setArtist(String artist) {
        mArtist = artist;
    }

    public Long getArtistId() {
        return mArtistId;
    }

    public void setArtistId(Long artistId) {
        mArtistId = artistId;
    }

    public String getBannerLink() {
        return mBannerLink;
    }

    public void setBannerLink(String bannerLink) {
        mBannerLink = bannerLink;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDownloadLink() {
        return mDownloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        mDownloadLink = downloadLink;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImageLink() {
        return mImageLink;
    }

    public void setImageLink(String imageLink) {
        mImageLink = imageLink;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mAlbumId);
        dest.writeString(this.mArtist);
        dest.writeValue(this.mArtistId);
        dest.writeString(this.mBannerLink);
        dest.writeString(this.mCreatedAt);
        dest.writeString(this.mDownloadLink);
        dest.writeValue(this.mId);
        dest.writeString(this.mImageLink);
        dest.writeString(this.mTitle);
    }

    public Track() {
    }

    protected Track(Parcel in) {
        this.mAlbumId = (Long) in.readValue(Long.class.getClassLoader());
        this.mArtist = in.readString();
        this.mArtistId = (Long) in.readValue(Long.class.getClassLoader());
        this.mBannerLink = in.readString();
        this.mCreatedAt = in.readString();
        this.mDownloadLink = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mImageLink = in.readString();
        this.mTitle = in.readString();
    }

    public static final Parcelable.Creator<Track> CREATOR = new Parcelable.Creator<Track>() {
        @Override
        public Track createFromParcel(Parcel source) {
            return new Track(source);
        }

        @Override
        public Track[] newArray(int size) {
            return new Track[size];
        }
    };
}
