
package com.ar.player.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class MusicVideo implements Parcelable {

    @SerializedName("artist")
    private String mArtist;
    @SerializedName("artist_id")
    private Long mArtistId;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("download_link")
    private String mDownloadLink;
    @SerializedName("id")
    private Long mId;
    @SerializedName("image_link")
    private String mImageLink;
    @SerializedName("title")
    private String mTitle;

    public String getArtist() {
        return mArtist;
    }

    public void setArtist(String artist) {
        mArtist = artist;
    }

    public Long getArtistId() {
        return mArtistId;
    }

    public void setArtistId(Long artistId) {
        mArtistId = artistId;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDownloadLink() {
        return mDownloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        mDownloadLink = downloadLink;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImageLink() {
        return mImageLink;
    }

    public void setImageLink(String imageLink) {
        mImageLink = imageLink;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mArtist);
        dest.writeValue(this.mArtistId);
        dest.writeString(this.mCreatedAt);
        dest.writeString(this.mDownloadLink);
        dest.writeValue(this.mId);
        dest.writeString(this.mImageLink);
        dest.writeString(this.mTitle);
    }

    public MusicVideo() {
    }

    protected MusicVideo(Parcel in) {
        this.mArtist = in.readString();
        this.mArtistId = (Long) in.readValue(Long.class.getClassLoader());
        this.mCreatedAt = in.readString();
        this.mDownloadLink = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mImageLink = in.readString();
        this.mTitle = in.readString();
    }

    public static final Parcelable.Creator<MusicVideo> CREATOR = new Parcelable.Creator<MusicVideo>() {
        @Override
        public MusicVideo createFromParcel(Parcel source) {
            return new MusicVideo(source);
        }

        @Override
        public MusicVideo[] newArray(int size) {
            return new MusicVideo[size];
        }
    };
}
